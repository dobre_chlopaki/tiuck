public interface Subtraction {
    public float subtract(float operand1, float operand2);
}
