import java.util.Scanner;

public class Calculator implements Addition, Subtraction, Division, Multiplication{
    private UserIO uio;
    private Scanner scanner;
    public Calculator(Scanner scanner){
        uio = new UserIO();
        this.scanner = scanner;
    }

    public int run() {
        float result;
        if(uio.run(scanner)>0) {
            System.out.println("Wystapil blad. Koniec programu.");
            return 1;
        }
        result = calculate(uio.getOperand1(), uio.getOperand2(), uio.getOperation());
        if(result == Float.MAX_VALUE)
            return 1;
        else{
            System.out.println("Wynik operacji: " + result);
        }
        return 0;
    }

    public float calculate(float operand1, float operand2, char operation){
        switch(operation){
            case '+':
                return add(operand1, operand2);
            case '-':
                return subtract(operand1, operand2);
            case '/':
                return divide(operand1, operand2);
            case '*':
                return multiply(operand1, operand2);
            default:
                return Float.MAX_VALUE;
        }

    }

    public float add(float operand1, float operand2) {
        return operand1 + operand2;
    }

    public float divide(float operand1, float operand2) {
        return operand1 / operand2;
    }

    public float multiply(float operand1, float operand2) {
        return operand1 * operand2;
    }

    public float subtract(float operand1, float operand2) {
        return operand1 - operand2;
    }

}
