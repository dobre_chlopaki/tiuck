import java.util.Scanner;

public class App {
    public static void main(String[] args){
        Calculator calc = new Calculator(new Scanner(System.in));
        calc.run();
    }
}
