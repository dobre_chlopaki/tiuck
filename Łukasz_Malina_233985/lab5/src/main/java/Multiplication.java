public interface Multiplication {
    public float multiply(float operand1, float operand2);
}
