import java.util.Scanner;

public class UserIO {
    private float operand1;
    private float operand2;
    private char operation;


    public int run(Scanner scanner){
        System.out.println("Podaj pierwszy operand: ");
        if(scanner.hasNextFloat())
            operand1 = scanner.nextFloat();
        else{
            System.out.println("Podano niepoprawny operand 1");
            return 1;
        }
        System.out.println("Podaj drugi operand: ");
        if(scanner.hasNextFloat())
            operand2 = scanner.nextFloat();
        else{
            System.out.println("Podano niepoprawny operand 2");
            return 2;
        }
        System.out.println("Podaj operacje (dostepne +, -, /, *): ");
        if(scanner.hasNext())
            operation = scanner.next().charAt(0);
        else{
            System.out.println("Podano niepoprawna operacje");
            return 3;
        }
        return 0;
    }

    public float getOperand1() {
        return operand1;
    }

    public float getOperand2() {
        return operand2;
    }

    public char getOperation() {
        return operation;
    }
}
