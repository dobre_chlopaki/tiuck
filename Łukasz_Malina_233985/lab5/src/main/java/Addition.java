public interface Addition {
    public float add(float operand1, float operand2);
}
