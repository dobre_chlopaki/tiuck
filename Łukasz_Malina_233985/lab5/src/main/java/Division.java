public interface Division {
    public float divide(float operand1, float operand2);
}
