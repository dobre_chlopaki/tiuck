import junit.framework.TestCase;
import org.junit.Test;
import org.junit.Rule;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.*;
import java.util.Scanner;

import static org.mockito.Mockito.when;

public class CalculatorTest extends TestCase{

    @Test
    public void testCalculateAdd(){
        Calculator calculator = new Calculator(new Scanner(System.in));
        float op1 = 5.67f;
        float op2 = 4.33f;
        char operation = '+';
        assertEquals(calculator.calculate(op1, op2, operation), 10.0f);
    }

    @Test
    public void testCalculateSubtract(){
        Calculator calculator = new Calculator(new Scanner(System.in));
        float op1 = 10.0f;
        float op2 = 4.33f;
        char operation = '-';
        assertEquals(calculator.calculate(op1, op2, operation), 5.67f);
    }

    @Test
    public void testCalculateDivide(){
        Calculator calculator = new Calculator(new Scanner(System.in));
        float op1 = 8f;
        float op2 = 4f;
        char operation = '/';
        assertEquals(calculator.calculate(op1, op2, operation), 2f);
    }

    @Test
    public void testCalculateMultiply(){
        Calculator calculator = new Calculator(new Scanner(System.in));
        float op1 = 8f;
        float op2 = 4f;
        char operation = '*';
        assertEquals(calculator.calculate(op1, op2, operation), 32f);
    }

    @Test
    public void testCalculateWrongOperation(){
        Calculator calculator = new Calculator(new Scanner(System.in));
        float op1 = 8f;
        float op2 = 4f;
        char operation = 'W';
        assertEquals(calculator.calculate(op1, op2, operation), Float.MAX_VALUE);
    }

    @Test
    public void testUserInputAllCorrect() {
        UserIO uio = new UserIO();

        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "4" + System.getProperty("line.separator")
                + "+";

        Scanner scanner = new Scanner(simulatedUserInput);

        assertEquals(uio.run(scanner), 0);
    }

    @Test
    public void testUserInputInvalidFirstOperand() {
        UserIO uio = new UserIO();

        String simulatedUserInput = "G" + System.getProperty("line.separator")
                + "4" + System.getProperty("line.separator")
                + "+";

        Scanner scanner = new Scanner(simulatedUserInput);

        assertEquals(uio.run(scanner), 1);
    }

    @Test
    public void testUserInputInvalidSecondOperand() {
        UserIO uio = new UserIO();

        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "S" + System.getProperty("line.separator")
                + "+";

        Scanner scanner = new Scanner(simulatedUserInput);

        assertEquals(uio.run(scanner), 2);
    }


    @Test
    public void testUserInputInvalidOperation() {
        UserIO uio = new UserIO();

        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "6" + System.getProperty("line.separator")
                + "";

        Scanner scanner = new Scanner(simulatedUserInput);

        assertEquals(uio.run(scanner), 3);
    }

    @Test
    public void testRun(){
        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "5" + System.getProperty("line.separator")
                + "+";

        Scanner scanner = new Scanner(simulatedUserInput);

        Calculator calculator = new Calculator(scanner);

        assertEquals(calculator.run(), 0);
    }

    @Test
    public void testRunInvalidInput(){
        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "G" + System.getProperty("line.separator")
                + "+";

        Scanner scanner = new Scanner(simulatedUserInput);

        Calculator calculator = new Calculator(scanner);

        assertEquals(calculator.run(), 1);
    }

    @Test
    public void testRunInvalidOperand(){
        String simulatedUserInput = "5" + System.getProperty("line.separator")
                + "5" + System.getProperty("line.separator")
                + "";

        Scanner scanner = new Scanner(simulatedUserInput);

        Calculator calculator = new Calculator(scanner);

        assertEquals(calculator.run(), 1);
    }

}
