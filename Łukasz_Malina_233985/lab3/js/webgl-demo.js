var cubeRotation = 0.0;

main();

function main() {
  const canvas = document.querySelector('#glcanvas');
  const gl = canvas.getContext('webgl');
  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }
  const vertexShader = getShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  const fragmentShader = getShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
  const shaderProgram = initShaderProgram(gl, vertexShaderSource, fragmentShaderSource, vertexShader, fragmentShader);
  const programAttribs = [
	  'aVertexPosition',
	  'aVertexNormal',
	  'aTextureCoord',
	  'uProjectionMatrix',
	  'uModelViewMatrix',
	  'uNormalMatrix',
	  'uSampler'
	  ];
  const programInfo = initProgramInfo(gl, shaderProgram, programAttribs);
  const buffers = initBuffers(gl);
  const cubeTexture = getTextureFromFile(gl, 'img/cubetexture.png');
  var then = 0;
  function render(now) {
    now *= 0.001;
    const deltaTime = now - then;
    then = now;
    drawScene(gl, programInfo, buffers, cubeTexture, deltaTime);
    requestAnimationFrame(render);
  }
  requestAnimationFrame(render);
}