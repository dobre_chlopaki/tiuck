public interface Operations {
    double add(double res, int num);
    double subtract(double res,int num);
    double multiply(double res,int num);
    double divide(double res, int num);
}
