import java.util.*;

public class Calculator {

    private List<Character> listOfAvailableSigns = Arrays.asList('+', '-', '*', '/');
    private Set<Character> availableSigns = new HashSet<Character>(listOfAvailableSigns);

    public ArrayList<Character> getMathOperators(String equasion) {
        ArrayList<Character> operators = new ArrayList<Character>();
        for (int i = 0; i < equasion.length(); i++) {
            char character = equasion.charAt(i);
            if (availableSigns.contains(character)) {
                operators.add(character);
            }
        }
        return operators;
    }

    public ArrayList<Integer> getNumbers(String equasion) {
        String newEquasion = equasion.replace("+", "&").replace("-", "&")
                .replace("*", "&").replace("/", "&");
        String[] stringNumbers = newEquasion.split("&");
        ArrayList<Integer> intNumbers = new ArrayList<Integer>();
        for (int i = 0; i < stringNumbers.length; i++) {
            String curentNumber = stringNumbers[i];
            intNumbers.add(Integer.parseInt(curentNumber));
        }
        return intNumbers;
    }

    public boolean isEquasionValid(String equasion) {
        int lastIndex = equasion.length() - 1;
        char lastSign = equasion.charAt(lastIndex);
        char firstSign = equasion.charAt(0);

        boolean hasLastSign = lastSign == '+' || lastSign == '-' || lastSign == '*' || lastSign == '/';
        boolean hasFirstSign = firstSign == '+' || firstSign == '-' || firstSign == '*' || firstSign == '/';

        return !hasFirstSign && !hasLastSign;
    }

    public double performOperation(int nextNumber, double result, char sign) {

        Operations operations = new BasicOperations();
        switch (sign) {
            case '+':
                result = operations.add(result, nextNumber);
                break;
            case '-':
                result = operations.subtract(result, nextNumber);
                break;
            case '*':
                result = operations.multiply(result, nextNumber);
                break;
            case '/':
                try {
                    result = operations.divide(result, nextNumber);
                } catch (ArithmeticException e) {
                    System.out.println(e);
                }
        }
        return result;
    }

    public double performCalculations(String equasion) {
        ArrayList<Integer> numbers = getNumbers(equasion);
        ArrayList<Character> signs = getMathOperators(equasion);

        /* Something wrong with given equasion */
        if (!isEquasionValid(equasion) || numbers.size() != signs.size() + 1) {
            System.out.println("Wrong equasion ");
        }

        double result = numbers.get(0);
        char currentSign;
        int nextNumber;
        for (int i = 0; i < signs.size(); i++) {
            nextNumber = numbers.get(i + 1);
            currentSign = signs.get(i);
            result = performOperation(nextNumber, result, currentSign);

        }

        System.out.println("Result is " + result);
        return result;

    }
}
