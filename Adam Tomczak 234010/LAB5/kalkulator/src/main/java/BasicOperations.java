public class BasicOperations implements Operations {
    @Override
    public double add(double res, int num) {
        return res + num;
    }

    @Override
    public double subtract(double res, int num) {
        return res - num;
    }

    @Override
    public double multiply(double res, int num) {
        return res * num;
    }

    @Override
    public double divide(double res, int num) throws ArithmeticException{
        double result = res / num;;
        if(result ==0){
            throw new ArithmeticException();
        }
        return result;
    }
}
