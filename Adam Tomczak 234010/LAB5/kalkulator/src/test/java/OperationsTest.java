import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class OperationsTest {
    Operations operations = new BasicOperations();

    double sum = operations.add(10.0,12);
    double difference = operations.subtract(100.0,22);
    double product = operations.multiply(100.0,5);
    double quotient = operations.divide(100.0,2);

    @Test
    public void isCorrectSum(){
        assertEquals(sum, 22.0);
    }

    @Test
    public void isCorrectDifference(){
        assertEquals(difference, 78.0);
    }

    @Test
    public void isCorrectProduct(){
        assertEquals(product, 500.0);
    }

    @Test
    public void isCorrectQuotient(){
        assertEquals(quotient, 50.0);
    }
}
