import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;

public class CalculatorTest {
    Calculator calculator = new Calculator();
    String equasion = "10+22-12";

    double resultTest1 = calculator.performCalculations(equasion);
    double resultTest2 = calculator.performCalculations("12*10/2");

    double sumTest = calculator.performOperation(12,1.2,'+');
    double differenceTest = calculator.performOperation(10,24.0,'-');
    double productTest = calculator.performOperation(2,24.0,'*');
    double quotientTest = calculator.performOperation(100,1200.0,'/');

    ArrayList<Integer> getNums(){
        ArrayList<Integer> nums = new ArrayList<Integer>();
        nums.add(10);
        nums.add(22);
        nums.add(12);
        return nums;
    }

    ArrayList<Character> getCharacters(){
        ArrayList<Character> characters = new ArrayList<Character>();
        characters.add('+');
        characters.add('-');
        return characters;
    }

    ArrayList<Character> characters = calculator.getMathOperators(equasion);
    ArrayList<Integer> numbers = calculator.getNumbers(equasion);

    @Test
    public void isCorrectResult(){
        assertEquals(resultTest1, 20.0);
        assertEquals(resultTest2, 60.0);
    }

    @Test
    public void isCorrectOperation(){
        assertEquals(sumTest, 13.2);
        assertEquals(differenceTest, 14.0);
        assertEquals(productTest, 48.0);
        assertEquals(quotientTest, 12.0);
    }

    @Test
    public void correctSigns(){
        assertEquals(characters, getCharacters());
    }

    @Test
    public void correctNumbers(){
        assertEquals(numbers,getNums());
    }

    @Test
    public void isCorrectEquasion(){
        assertEquals(calculator.isEquasionValid(equasion), true);
        assertEquals(calculator.isEquasionValid("*12+1"), false);
        assertEquals(calculator.isEquasionValid("12+1-"), false);
    }

}
