var cubeRotation = 0.0;
main();

function main() {
  const canvas = document.querySelector("#glcanvas");
  const gl = canvas.getContext("webgl");

  // If we don't have a GL context, give up now
  if (!gl) {
    alert(
      "Unable to initialize WebGL. Your browser or machine may not support it."
    );
    return;
  }
  // Initialize a shader program; this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgram = new ShaderProgram().init(gl);

  // Collect all the info needed to use the shader program.
  const programInfo = getShaderAttributes(shaderProgram, gl);

  // Here's where we call the routine that builds all the objects we'll be drawing.
  const buffers = new BufferHolder().init(gl);
  const texture = new Texture().load(gl, "cubetexture.png");

  // Draw the scene repeatedly
  render = new Renderer(gl, programInfo, buffers, texture).startRender();
}
