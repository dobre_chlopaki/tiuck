class Renderer {
  constructor(gl, programInfo, buffers, texture) {
    this.gl = gl;
    this.programInfo = programInfo;
    this.buffers = buffers;
    this.texture = texture;
  }
  startRender() {
    var then = 0;
    var gl = this.gl;
    var programInfo = this.programInfo;
    var buffers = this.buffers;
    var texture = this.texture;

    function render(now) {
      now = convertToSeconds(now); 
      const deltaTime = now - then;
      then = now;

      new SceneDrawer().drawScene(gl, programInfo, buffers, texture, deltaTime);
      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
  }
}
