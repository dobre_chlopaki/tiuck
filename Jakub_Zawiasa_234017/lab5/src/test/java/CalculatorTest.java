import com.lab5.io.Calculator;
import org.junit.Test;
import  com.lab5.io.Input;
import junit.framework.TestCase;
import org.mockito.Mockito;


import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;


import java.io.ByteArrayInputStream;

import static org.mockito.Mockito.when;

public class CalculatorTest extends TestCase{

    @Test
    public void testOperationAddition() {

        Calculator calculator = new Calculator();
        double a = 5, b = 6;
        int op = 1;

        assertEquals(calculator.calculate(a, b, op), a + b);
    }

    @Test
    public void testOperationSubstraction() {

        Calculator calc = new Calculator();
        double a = 5, b = 6;
        int op = 2;

        assertEquals(calc.calculate(a, b, op), a - b);
    }

    @Test
    public void testOperationMultiplication() {

        Calculator calc = new Calculator();
        double a = 5, b = 6;
        int op = 3;

        assertEquals(calc.calculate(a, b, op), a * b);
    }

    @Test
    public void testOperationDivision() {

        Calculator calc = new Calculator();
        double a = 5, b = 6;
        int op = 4;

        assertEquals(calc.calculate(a, b, op), a / b);
    }

    @Test
    public void testOperationInvalid() {

        Calculator calc = new Calculator();
        double a = 5, b = 6;
        int op = 0;

        assertEquals(calc.calculate(a, b, op), 0.0);
    }
    @Test
    public void testIfCanRunCalculator() {
        Input mockInput = Mockito.mock(Input.class);
        String[] args = new String[] {};
        when(mockInput.getNumber()).thenReturn(2.0);
        when(mockInput.getOperation()).thenReturn(1);
        new Calculator().run(mockInput);
    }
    @Test
    public void testIfStopOnUnsupportedOperation() {
        Input mockInput = Mockito.mock(Input.class);
        when(mockInput.getNumber()).thenReturn(2.0);
        when(mockInput.getOperation()).thenReturn(8);
        Calculator calculator = new Calculator();
        boolean result = calculator.run(mockInput);
        assertEquals(result, false);
    }
    @Test
    public void testGetOperation() {
        Input scanner = new Input();
        ByteArrayInputStream in = new ByteArrayInputStream("1".getBytes());
        System.setIn(in);
        assertEquals((int)1, (int)scanner.getOperation());
    }
    @Test
    public void testGetNumber() {
        Input scanner = new Input();
        ByteArrayInputStream in = new ByteArrayInputStream("1".getBytes());
        System.setIn(in);
        assertEquals(1, (int) scanner.getNumber());
    }
}
