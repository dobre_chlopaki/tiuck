import com.lab5.operations.AddOperation;

import com.lab5.operations.DivOperation;
import com.lab5.operations.MulOperation;
import junit.framework.TestCase;
import org.junit.Test;

public class OperationsTest extends TestCase {

    @Test
    public void testAddCalculate() {
        AddOperation operation = new AddOperation();

        double a = 1, b = 1;
        double c = a + b;

        assertEquals(c, operation.calculate(a, b));
    }
    @Test
    public void testMulCalculate() {
        MulOperation operation = new MulOperation();

        double a = 1, b = 1;
        double c = a * b;

        assertEquals(c, operation.calculate(a, b));
    }
    @Test
    public void testDivCalculate() {
        DivOperation operation = new DivOperation();

        double a = 1, b = 1;
        double c = a / b;

        assertEquals(c, operation.calculate(a, b));
    }
}