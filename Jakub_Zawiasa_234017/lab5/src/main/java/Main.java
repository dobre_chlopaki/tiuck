import com.lab5.io.Calculator;
import com.lab5.io.Input;

public class Main {

    public static Input scanners = new Input();

    public static void main(String[] args) {
        new Calculator().run(scanners);
    }
}