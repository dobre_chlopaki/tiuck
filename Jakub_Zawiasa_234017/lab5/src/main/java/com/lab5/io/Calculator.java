package com.lab5.io;

import com.lab5.operations.AddOperation;
import com.lab5.operations.DivOperation;
import com.lab5.operations.MulOperation;
import com.lab5.operations.SubOperation;


public class Calculator {
    public Calculator() {
    }
    Input io;
    public boolean run(Input scanner) {
        io = scanner;
        double a = io.getNumber();
        double b = io.getNumber();
        int choose = io.getOperation();
        double result = calculate(a, b, choose);
        if (result == 0) {
            System.out.println("Insert proper value (1-4)");
            return false;
        } else {
            System.out.println("Result: " + result);
        }
        System.out.println("Program has ended successfully!");
        return true;
    }

    public double calculate(double a, double b, int choose) {

        switch (choose) {
            case 1:
                return new AddOperation().calculate(a, b);
            case 2:
                return new SubOperation().calculate(a, b);
            case 3:
                return new MulOperation().calculate(a, b);
            case 4:
                return new DivOperation().calculate(a, b);
            default:
                return 0;

        }
    }
}
