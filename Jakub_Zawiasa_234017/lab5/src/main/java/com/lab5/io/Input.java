package com.lab5.io;

import java.io.InputStream;
import java.util.Scanner;

public class Input {

    public Scanner inp;

    public double getNumber() {
        System.out.println("Enter number:");
        inp = new Scanner(System.in);
        double a = inp.nextDouble();
        return a;
    }

    public int getOperation() {
        int choose;
        System.out.println("Enter your selection: 1 to add\n" +
                "2 to substract \n" +
                "3 for Multiply \n " +
                "4 for divide\n");
        inp = new Scanner(System.in);
        choose = inp.nextInt();
        return choose;
    }
}
