package com.lab5.operations;


public class AddOperation implements Operation {

    public double calculate(double a, double b) {
        return a + b;
    }

}
