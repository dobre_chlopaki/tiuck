package com.lab5.operations;

public class DivOperation implements Operation {

    public double calculate(double a, double b) {
        return a / b;
    }

}
