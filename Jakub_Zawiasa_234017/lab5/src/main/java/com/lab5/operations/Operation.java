package com.lab5.operations;


public interface Operation {

    double calculate(double a, double b);
}
