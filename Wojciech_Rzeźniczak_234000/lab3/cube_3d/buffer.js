
import * as BufferParams from './params/buffer-params.js';

export default function initBuffers(gl) {

    const positionBuffer = initPositionBuffer(gl);
    const normalBuffer = initNormalBuffer(gl);
    const textureCoordBuffer = initTextureCoordBuffer(gl);
    const indexBuffer = initIndexBuffer(gl);

    return {
        position: positionBuffer,
        normal: normalBuffer,
        textureCoord: textureCoordBuffer,
        indices: indexBuffer,
    };
}

function initIndexBuffer(gl) {
    const { indices } = BufferParams

    const indexBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indices), gl.STATIC_DRAW);

    return indexBuffer;
}

function initTextureCoordBuffer(gl) {
    const { textureCoordinates } = BufferParams

    const textureCoordBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
        gl.STATIC_DRAW);

    return textureCoordBuffer;
}

function initNormalBuffer(gl) {
    const { vertexNormals } = BufferParams

    const normalBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals),
        gl.STATIC_DRAW);

    return normalBuffer;
}

function initPositionBuffer(gl) {
    const { positions } = BufferParams

    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    return positionBuffer;
}
