export const vertex_shader_program = `
attribute vec4 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aTextureCoord;

uniform mat4 uNormalMatrix;
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

varying highp vec2 vTextureCoord;
varying highp vec3 vLighting;

void main(void) {
  gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
  vTextureCoord = aTextureCoord;

  // Apply lighting effect

  highp vec3 ambientLight = vec3(0.3, 0.3, 0.3);
  highp vec3 directionalLightColor = vec3(1, 1, 1);
  highp vec3 directionalVector = normalize(vec3(0.85, 0.8, 0.75));

  highp vec4 transformedNormal = uNormalMatrix * vec4(aVertexNormal, 1.0);

  highp float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);
  vLighting = ambientLight + (directionalLightColor * directional);
}
`;

export const fragment_shader_program = `
varying highp vec2 vTextureCoord;
varying highp vec3 vLighting;

uniform sampler2D uSampler;

void main(void) {
  highp vec4 texelColor = texture2D(uSampler, vTextureCoord);

  gl_FragColor = vec4(texelColor.rgb * vLighting, texelColor.a);
}
`;

export const shaderProgramInfoAttributes = {
  vertexPosition: 'aVertexPosition',
  vertexNormal: 'aVertexNormal',
  textureCoord:  'aTextureCoord',
  projectionMatrix: 'uProjectionMatrix',
  modelViewMatrix: 'uModelViewMatrix',
  normalMatrix: 'uNormalMatrix',
  uSampler: 'uSampler'
}

export const clearBackgorund = {
    blackColor: [0.0, 0.0, 0.0, 1.0],
    clearDepth: 1.0
}

export const projectionMatrixAttributes = {
  fieldOfView: 45 * Math.PI / 180,
  zNear: 0.1,
  zFar: 100.0
}

export const postionBufferIntoVertexPositionAttributes = {
  numComponents: 3,
  normalize: false,
  stride: 0,
  offset: 0
}

export const bufferPostionIntoVertexPositionAttributes = {
  numComponents: 2,
  normalize: false,
  stride: 0,
  offset: 0
}

export const normalBufferCoordinateIntoVertexNormalAttributes = {
  numComponents: 3,
  normalize: false,
  stride: 0,
  offset: 0
}

export const texture0Configuration = {
  vertexCount: 36,
  offset: 0,
  textureIndex: 0
}

export const defaultRenderFrameViewAttributes = {
  viewTranlation: [-0.0, 0.0, -6.0],
  zRotation: [0, 0, 1],
  xRotation: [0, 1, 0]
}
