import * as TextureLoaderParams from './params/texture-loader-params.js'

export default function loadTexture(webgl, textureURL) {

    const emptyTexture = webgl.createTexture();
    const texture = bindNewTexture(webgl, emptyTexture);

    const image = new Image();
    image.src = textureURL;

    dummyTexture(webgl);
    bindTextureToImage(webgl, texture, image);

    return texture;
}


function bindTextureToImage(webgl, texture, image) {
    const { level } = TextureLoaderParams.texImage2D;
    const { RGBA: srcFormat, RGBA: internalFormat, UNSIGNED_BYTE: srcType } = webgl;

    image.onload = () => {
        webgl.bindTexture(webgl.TEXTURE_2D, texture);
        webgl.texImage2D(webgl.TEXTURE_2D, level, internalFormat,
            srcFormat, srcType, image);

        if (checkIfImageSizeIsPowerOf2(image)) {
            generateMipmap(webgl)
        }
        else {
            wrappingToClampToEdge(webgl)
        }
    }
}

function dummyTexture(webgl) {
    const { level, width, height, border, pixel } = TextureLoaderParams.texImage2D;
    const { RGBA: srcFormat, RGBA: internalFormat, UNSIGNED_BYTE: srcType } = webgl;

    webgl.texImage2D(webgl.TEXTURE_2D, level, internalFormat,
        width, height, border, srcFormat, srcType,
        pixel);
}

function bindNewTexture(webgl, texture) {
    webgl.bindTexture(webgl.TEXTURE_2D, texture);
    return texture;
}

function generateMipmap(webgl) {
    return webgl.generateMipmap(webgl.TEXTURE_2D);
}

function wrappingToClampToEdge(webgl) {
    webgl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_WRAP_S, webgl.CLAMP_TO_EDGE);
    webgl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_WRAP_T, webgl.CLAMP_TO_EDGE);
    webgl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MIN_FILTER, webgl.LINEAR);
}

function checkIfImageSizeIsPowerOf2(image) {
    return isPowerOf2(image.width) && isPowerOf2(image.height);
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
}

