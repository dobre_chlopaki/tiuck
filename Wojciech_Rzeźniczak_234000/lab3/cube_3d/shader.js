import * as ShaderParams from './params/shader-params.js';
import initBuffers from './buffer.js'
import loadTexture from './texture-loader.js'



export class Shader {

  constructor(webgl, textureURL) {
    this.webgl = webgl;
    this.textureURL = textureURL;
  }

  initShader() {
    const { webgl, textureURL } = this;

    const shaderProgram = this.initShaderProgram();
    const shaderProgramInfo = this.initShaderProgramInfo(shaderProgram);
    const buffers = initBuffers(webgl);
    const texture = loadTexture(webgl, textureURL);

    this.shaderProgramInfo = shaderProgramInfo;
    this.buffers = buffers;
    this.texture = texture;
  }

  initShaderProgramInfo(shaderProgram) {
    const { webgl } = this
    const { shaderProgramInfoAttributes } = ShaderParams

    return {
      program: shaderProgram,
      attribLocations: {
        vertexPosition: webgl.getAttribLocation(shaderProgram, shaderProgramInfoAttributes.vertexPosition),
        vertexNormal: webgl.getAttribLocation(shaderProgram, shaderProgramInfoAttributes.vertexNormal),
        textureCoord: webgl.getAttribLocation(shaderProgram, shaderProgramInfoAttributes.textureCoord),
      },
      uniformLocations: {
        projectionMatrix: webgl.getUniformLocation(shaderProgram, shaderProgramInfoAttributes.projectionMatrix),
        modelViewMatrix: webgl.getUniformLocation(shaderProgram, shaderProgramInfoAttributes.modelViewMatrix),
        normalMatrix: webgl.getUniformLocation(shaderProgram, shaderProgramInfoAttributes.normalMatrix),
        uSampler: webgl.getUniformLocation(shaderProgram, shaderProgramInfoAttributes.uSampler),
      },
    }
  }

  initShaderProgram() {
    const { webgl } = this
    const { vertex_shader_program, fragment_shader_program } = ShaderParams

    const vertexShader = this.loadShader(webgl, webgl.VERTEX_SHADER, vertex_shader_program);
    const fragmentShader = this.loadShader(webgl, webgl.FRAGMENT_SHADER, fragment_shader_program);

    const shaderProgram = webgl.createProgram();
    webgl.attachShader(shaderProgram, vertexShader);
    webgl.attachShader(shaderProgram, fragmentShader);
    webgl.linkProgram(shaderProgram);

    if (!webgl.getProgramParameter(shaderProgram, webgl.LINK_STATUS)) {
      alert('Unable to initialize the shader program: ' + webgl.getProgramInfoLog(shaderProgram));
      return null;
    }

    return shaderProgram;
  }

  loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
    }

    return shader;
  }

}
export class ShaderRenderer {
  milisecondRatio = 0.001;
  
  constructor(renderableObject) {
    this.renderableObject = renderableObject;

    this.renderIntervalID = undefined;
    this.lastFrameTime = 0;
    

    this.render = this.render.bind(this);
  }

  startRendering(refreshRate) {
    this.renderIntervalID = setInterval(() => requestAnimationFrame(this.render), refreshRate);
  }

  stopRendering() {
    clearInterval(this.renderIntervalID);
  }

  render(nextFrameTime) {
    let { lastFrameTime, milisecondRatio } = this

    nextFrameTime *= milisecondRatio
    const deltaTime = nextFrameTime - lastFrameTime;
    this.lastFrameTime = nextFrameTime;
    
    this.renderableObject.drawFrame(deltaTime);
  }
}

export class Renderable {
  drawFrame(deltaTime){

  }
}

export class Cube extends Renderable {
  rotationTotalTime = 0.0;

  constructor(shader){
    super()
    this.shader = shader
    this.buffers = shader.buffers
    this.shaderProgramInfo= shader.shaderProgramInfo

    this.viewTranlation = ShaderParams.defaultRenderFrameViewAttributes.viewTranlation;
    this.zRotation = ShaderParams.defaultRenderFrameViewAttributes.zRotation;
    this.xRotation = ShaderParams.defaultRenderFrameViewAttributes.xRotation;

    this.drawFrame = this.drawFrame.bind(this);
  }

  drawFrame(deltaTime) {
    this.clearBackground()
    this.clearCanvas()

    const projectionMatrix = this.initProjectionMatrix()
    const modelViewMatrix = this.initModelViewMatrix()
    const normalMatrix = this.initNormalMatrix(modelViewMatrix)

    this.setBufferPostionIntoVertexPosition()
    this.setTextureCoordinateIntoTextureCoord()
    this.setNormalBufferCoordinateIntoVertexNormal()

    this.setIndicesToBuffer()
    this.setShaderProgramToDraw()

    const { projectionMatrix: projectionMatrixLocation, modelViewMatrix: modelViewMatrixLocation, normalMatrix: normalMatrixLocation }
      = this.shaderProgramInfo.uniformLocations
    this.uniformMatrixWithShaderUniform(projectionMatrix, projectionMatrixLocation)
    this.uniformMatrixWithShaderUniform(modelViewMatrix, modelViewMatrixLocation)
    this.uniformMatrixWithShaderUniform(normalMatrix, normalMatrixLocation)

    this.setTexture0Configuration()
    
    this.updateRotationTotalTime(deltaTime)
  }

  clearBackground() {
    const { webgl } = this.shader
    const { blackColor, clearDepth } = ShaderParams.clearBackgorund
    const { red, green, blue, alpha } = blackColor

    webgl.clearColor(red, green, blue, alpha);
    webgl.clearDepth(clearDepth);
  }

  clearCanvas() {
    const { webgl } = this.shader

    webgl.enable(webgl.DEPTH_TEST);
    webgl.depthFunc(webgl.LEQUAL);
    webgl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
  }

  initProjectionMatrix() {
    const { webgl } = this.shader
    const { fieldOfView, zNear, zFar } = ShaderParams.projectionMatrixAttributes

    const aspect = webgl.canvas.clientWidth / webgl.canvas.clientHeight;

    const projectionMatrix = mat4.create();

    mat4.perspective(projectionMatrix,
      fieldOfView,
      aspect,
      zNear,
      zFar);

    return projectionMatrix
  }

  //TODO
  initModelViewMatrix() {
    const { rotationTotalTime: cubeRotationRatio, viewTranlation, zRotation, xRotation } = this

    const modelViewMatrix = mat4.create();

    mat4.translate(modelViewMatrix,     // destination matrix
      modelViewMatrix,     // matrix to translate
      viewTranlation);  // amount to translate
    mat4.rotate(modelViewMatrix,  // destination matrix
      modelViewMatrix,  // matrix to rotate
      cubeRotationRatio,     // amount to rotate in radians
      zRotation);       // axis to rotate around (Z)
    mat4.rotate(modelViewMatrix,  // destination matrix
      modelViewMatrix,  // matrix to rotate
      cubeRotationRatio * .7,// amount to rotate in radians
      xRotation);       // axis to rotate around (X)

    return modelViewMatrix

  }

  initNormalMatrix(modelViewMatrix) {
    const normalMatrix = mat4.create();
    mat4.invert(normalMatrix, modelViewMatrix);
    mat4.transpose(normalMatrix, normalMatrix);
    return normalMatrix
  }

  setVertexFromBuffer({ bufferPosition, numComponents, type, normalize, stride, offset, vertexPosition }) {
    const { webgl } = this.shader

    webgl.bindBuffer(webgl.ARRAY_BUFFER, bufferPosition);
    webgl.vertexAttribPointer(
      vertexPosition,
      numComponents,
      type,
      normalize,
      stride,
      offset);
    webgl.enableVertexAttribArray(vertexPosition);
  }

  setBufferPostionIntoVertexPosition() {
    const { webgl, buffers, shaderProgramInfo } = this.shader

    const attributes = ShaderParams.postionBufferIntoVertexPositionAttributes;
    attributes.type = webgl.FLOAT;
    attributes.bufferPosition = buffers.position;
    attributes.vertexPosition = shaderProgramInfo.attribLocations.vertexPosition;

    this.setVertexFromBuffer(attributes)
  }

  setTextureCoordinateIntoTextureCoord() {
    const { webgl, buffers, shaderProgramInfo  } = this.shader

    const attributes = ShaderParams.bufferPostionIntoVertexPositionAttributes;
    attributes.type = webgl.FLOAT;
    attributes.bufferPosition = buffers.textureCoord;
    attributes.vertexPosition = shaderProgramInfo.attribLocations.textureCoord;

    this.setVertexFromBuffer(attributes)
  }

  setNormalBufferCoordinateIntoVertexNormal() {
    const { webgl, buffers, shaderProgramInfo } = this.shader

    const attributes = ShaderParams.normalBufferCoordinateIntoVertexNormalAttributes;
    attributes.type = webgl.FLOAT;
    attributes.bufferPosition = buffers.normal;
    attributes.vertexPosition = shaderProgramInfo.attribLocations.vertexNormal;

    this.setVertexFromBuffer(attributes)
  }

  setIndicesToBuffer() {
    const { webgl, buffers } = this.shader

    webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, buffers.indices);
  }

  setShaderProgramToDraw() {
    const { webgl, shaderProgramInfo } = this.shader

    webgl.useProgram(shaderProgramInfo.program);
  }

  uniformMatrixWithShaderUniform(matrix, shaderUniform) {
    const { webgl } = this.shader
    webgl.uniformMatrix4fv(shaderUniform, false, matrix);
  }

  setTexture0Configuration() {
    const { webgl, texture, shaderProgramInfo } = this.shader
    const { vertexCount, offset, textureIndex } = ShaderParams.texture0Configuration

    const type = webgl.UNSIGNED_SHORT;

    webgl.activeTexture(webgl.TEXTURE0);
    webgl.bindTexture(webgl.TEXTURE_2D, texture);
    webgl.uniform1i(shaderProgramInfo.uniformLocations.uSampler, textureIndex);
    webgl.drawElements(webgl.TRIANGLES, vertexCount, type, offset);

  }

  updateRotationTotalTime(deltaTime) {
    this.rotationTotalTime += deltaTime;
  }


}


