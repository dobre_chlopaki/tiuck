

import { Shader, ShaderRenderer, Cube } from './shader.js'

window.onload = function main() {
  const canvas = document.querySelector('#glcanvas');
  const webgl = canvas.getContext('webgl');


  if (!webgl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  const shader = new Shader(webgl, '../images/cubetexture.png')
  shader.initShader()

  const cube = new Cube(shader)
  const shaderRenderer = new ShaderRenderer(cube)

  shaderRenderer.startRendering(50)
  
  // shaderRenderer.stopRendering()
}
