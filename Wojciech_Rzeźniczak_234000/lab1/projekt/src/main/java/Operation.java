public interface Operation {
    float count(float a, float b);
}
