/** Calculator class which calculate numbers without instance */
public class Calculator {

    /**
     * Count sum of numbers
     * @param numbers
     *            numbers to sum
     * @return sum of numbers
     */
    public static float sum(float... numbers) {
        Operation operation = (a, b) -> a + b;
        return makeOperation(operation, numbers);
    }

    /**
     * Count subtraction of numbers
     * @param numbers
     *            numbers to subtract
     * @return subtraction of numbers
     */
    public static float subtraction(float... numbers) {
        Operation operation = (a, b) -> a - b;
        return makeOperation(operation, numbers);
    }

    /**
     * Count multiplication of numbers
     * @param numbers
     *            numbers to multiply
     * @return multiplication of numbers
     */
    public static float multiplication(float... numbers) {
        Operation operation = (a, b) -> a * b;
        return makeOperation(operation, numbers);
    }

    /**
     * Count division of numbers
     * @param numbers
     *            numbers to divide
     * @return division of numbers
     */
    public static float division(float... numbers) {
        Operation operation = (a, b) -> a / b;
        return makeOperation(operation, numbers);
    }

    /**
     * Counts number for given operation
     * @param operation
     *            interface which count numbers
     * @param numbers
     *            number to count
     * @return counted numbers
     */
    private static float makeOperation(Operation operation, float... numbers) {
        float result = numbers[0];
        for (int i = 1; i < numbers.length; i++)
            result = operation.count(result, numbers[i]);
        return result;
    }
}
